/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package allbanks_parser;

import com.gargoylesoftware.htmlunit.javascript.host.Console;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NotFoundException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.owasp.html.HtmlPolicyBuilder;
import org.owasp.html.HtmlSanitizer;
import org.owasp.html.PolicyFactory;
import org.owasp.html.Sanitizers;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 *
 * @author R5
 */
public class Allbanks_parser {

    static JavascriptExecutor js;

    public static String getTextExclChild(WebDriver driver, WebElement element) {
        if (driver instanceof JavascriptExecutor) {
            js = (JavascriptExecutor) driver;
        }
        return (String) js.executeScript("var parent = arguments[0];\n"
                + "var child = parent.firstChild;\n"
                + "var ret = \"\";\n"
                + "while(child) {\n"
                + "    if (child.nodeType === Node.TEXT_NODE)\n"
                + "        ret += child.textContent;\n"
                + "    child = child.nextSibling;\n"
                + "}\n"
                + "return ret;", element);
    }

    public static boolean removeElementChildren(WebDriver driver, WebElement element) {
        if (driver instanceof JavascriptExecutor) {
            js = (JavascriptExecutor) driver;
        }
        return (boolean) js.executeScript("var rmvd = arguments[0];\n"
                + "var ret = false;\n"
                + "if (rmvd) {\n"
                + "rmvd.innerHTML=\"\";"
                + "ret = true;"
                + "}"
                + "return ret;", element);
    }

    public static String excludeLastLine(String x) {
        if (x.lastIndexOf("\n") > 0) {
            return x.substring(0, x.lastIndexOf("\n"));
        } else {
            return x;
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        /*
         try {

         DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
         DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

         // root elements
         Document doc = docBuilder.newDocument();
         Element rootElement = doc.createElement("company");
         doc.appendChild(rootElement);

         // staff elements
         Element staff = doc.createElement("Staff");
         rootElement.appendChild(staff);

         // set attribute to staff element
         Attr attr = doc.createAttribute("id");
         attr.setValue("1");
         staff.setAttributeNode(attr);

         // shorten way
         // staff.setAttribute("id", "1");
         // firstname elements
         Element firstname = doc.createElement("firstname");
         firstname.appendChild(doc.createTextNode("yong"));
         staff.appendChild(firstname);

         // lastname elements
         Element lastname = doc.createElement("lastname");
         lastname.appendChild(doc.createTextNode("mook kim"));
         staff.appendChild(lastname);

         // nickname elements
         Element nickname = doc.createElement("nickname");
         nickname.appendChild(doc.createTextNode("mkyong"));
         staff.appendChild(nickname);

         // salary elements
         Element salary = doc.createElement("salary");
         salary.appendChild(doc.createTextNode("100000"));
         staff.appendChild(salary);

         // write the content into xml file
         TransformerFactory transformerFactory = TransformerFactory.newInstance();
         Transformer transformer = transformerFactory.newTransformer();
         DOMSource source = new DOMSource(doc);
         StreamResult result = new StreamResult(new File("D:\\file.xml"));

         // Output to console for testing
         // StreamResult result = new StreamResult(System.out);
         transformer.transform(source, result);

         System.out.println("File saved!");

         } catch (ParserConfigurationException pce) {
         pce.printStackTrace();
         } catch (TransformerException tfe) {
         tfe.printStackTrace();
         }
         */

        //String safeHTML = policy.sanitize(bankCard.findElement(By.cssSelector("tr.start h1")).getText());
        //bank_name.appendChild(doc.createTextNode(safeHTML));
        try {
            //Подготовка драйвера
            WebDriver driver = null;
            ProfilesIni allProfiles = new ProfilesIni();
            FirefoxProfile profile = allProfiles.getProfile("SeleniumFF");
            FirefoxBinary binary = new FirefoxBinary(new File("C:\\Program Files (x86)\\Mozilla Firefox\\firefox.exe"));
            driver = new FirefoxDriver(binary, profile);
            if (driver instanceof JavascriptExecutor) {
                js = (JavascriptExecutor) driver;
            }

            //Подготовка XML
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            Document doc = docBuilder.newDocument();
            Element rootElement = doc.createElement("banklist");
            doc.appendChild(rootElement);

            //Санитайзер HTML
            PolicyFactory policy = Sanitizers.LINKS.and(Sanitizers.FORMATTING).and(new HtmlPolicyBuilder().disallowElements("table").disallowTextIn("table").toFactory());

            boolean thatFuckingTable;
            Element bank;
            Element bank_name, bank_location, bank_logo, bank_param, bank_param_sub_1, bank_param_sub_2, bank_param_sub_3;
            WebElement dom_bank_card, dom_bank_head, dom_bank_row_name, dom_bank_row_value/*, dom_leftmenu, dom_leftmenu_select, dom_leftmenu_select_sub*/;
            List<WebElement> dom_bank_info_rows, dom_bank_blocks;
            String tmpStr, tmpStr2;

            /*
            String[] banks = {"http://www.allbanks.ru/banks/russia/centre/moscow/9259/",
                "http://www.allbanks.ru/banks/russia/centre/moscow/195654/",
                "http://www.allbanks.ru/banks/tajikistan/1344723/"};
            */
            boolean continueFetchList=true;
            
            ArrayList<String> banks = new ArrayList<>();
            int i=1;
            int j=0;
            while (i<65/*continueFetchList*/) {
            driver.get("http://www.allbanks.ru/banks/page"+i);
                dom_bank_blocks = driver.findElements(By.cssSelector("div.content table.bankBlock h3 a:not([style])"));
                for (WebElement bank_block : dom_bank_blocks) {
                    j++;
                    banks.add(bank_block.getAttribute("href"));
                }
                System.out.println("Added banks: "+j);
                /*
                try {
                    driver.findElement(By.cssSelector("a[title=\"следующая\"]")).click();
                    Thread.sleep(1500);
                }
                catch (NotFoundException e) {
                    continueFetchList=true;
                }
                */
                Thread.sleep(1000);
                i++;
            }
            
            
            i=0;
            for (String bankUrl : banks) {
                i++;
                System.out.println(i+" | "+j);
                driver.get(bankUrl);
                dom_bank_card = driver.findElement(By.cssSelector("table.bankCard"));
                bank = doc.createElement("bank");
                //Хед (имя, локация, ?лого)
                bank_name = doc.createElement("name");
                dom_bank_head = dom_bank_card.findElement(By.cssSelector("tr.start"));
                bank_name.appendChild(doc.createTextNode(getTextExclChild(driver, dom_bank_head.findElement(By.cssSelector("h1")))));
                bank.appendChild(bank_name);
                bank_location = doc.createElement("location");
                bank_location.appendChild(doc.createTextNode(policy.sanitize(dom_bank_head.findElement(By.cssSelector("div:not([style])")).getText())));
                bank.appendChild(bank_location);
                try {
                    tmpStr = dom_bank_head.findElement(By.cssSelector("img")).getAttribute("src");
                    bank_logo = doc.createElement("logo");
                    bank_logo.appendChild(doc.createTextNode(tmpStr));
                    bank.appendChild(bank_logo);
                } catch (NotFoundException e) {
                }

                //Категория (страна, ?город)
                /*
                try {
                    dom_leftmenu = driver.findElement(By.cssSelector("ul.mainmenu"));
                    dom_leftmenu_select = dom_leftmenu.findElement(By.cssSelector("div.marked.markedTop,div.current.currentTop a.current"));
                    bank_param = doc.createElement("category");
                    bank_param_sub_1 = doc.createElement("country");
                    bank_param_sub_1.appendChild(doc.createTextNode(dom_leftmenu_select.getText()));
                    bank_param.appendChild(bank_param_sub_1);
                    try {
                        dom_leftmenu_select_sub = dom_leftmenu.findElement(By.cssSelector("li.select a.current"));
                        bank_param_sub_2 = doc.createElement("city");
                        bank_param_sub_2.appendChild(doc.createTextNode(dom_leftmenu_select_sub.getText()));
                        bank_param.appendChild(bank_param_sub_2);
                    } catch (NotFoundException e) {
                    }
                    bank.appendChild(bank_param);
                } catch (NotFoundException e) {
                }*/

                dom_bank_info_rows = dom_bank_card.findElements(By.cssSelector("tr:not(.start):not(.end)"));
                for (WebElement info_row : dom_bank_info_rows) {
                    try {
                        dom_bank_row_name = info_row.findElement(By.cssSelector("td.name"));
                        /*
                         bank_param = doc.createElement("name_"+i);
                         bank_param.appendChild(doc.createTextNode(getTextExclChild(driver,dom_bank_row_name)));
                         bank.appendChild(bank_param);
                         i++;
                         */
                        tmpStr2 = getTextExclChild(driver, dom_bank_row_name).trim();
                        dom_bank_row_value = info_row.findElement(By.cssSelector("td:not(.name)"));
                        switch (tmpStr2) {
                            case "Полное наименование":
                                bank_param = doc.createElement("full_name");
                                bank_param.appendChild(doc.createTextNode(getTextExclChild(driver, dom_bank_row_value)));
                                bank.appendChild(bank_param);
                                break;
                            case "Номер лицензии":
                                bank_param = doc.createElement("license_num");
                                bank_param.appendChild(doc.createTextNode(getTextExclChild(driver, dom_bank_row_value).replaceAll("[^0-9.]", "")));
                                bank.appendChild(bank_param);
                                try {
                                    bank_param = doc.createElement("cbrf_link");
                                    bank_param.appendChild(doc.createTextNode(dom_bank_row_value.findElement(By.tagName("a")).getAttribute("href")));
                                    bank.appendChild(bank_param);
                                } catch (NotFoundException e) {
                                }
                                break;
                            case "Руководитель исполнительного органа":
                                bank_param = doc.createElement("executive_head");
                                bank_param.appendChild(doc.createTextNode(getTextExclChild(driver, dom_bank_row_value).trim()));
                                bank_param_sub_1 = doc.createElement("last_check");
                                bank_param_sub_1.appendChild(doc.createTextNode(dom_bank_row_value.findElement(By.tagName("span")).getText().replaceAll("[^0-9.]", "")));
                                bank_param.appendChild(bank_param_sub_1);
                                bank.appendChild(bank_param);
                                break;
                            case "Контакты":
                                boolean hasContacts = false;
                                bank_param = doc.createElement("contact");
                                try {
                                    bank_param_sub_1 = doc.createElement("address");
                                    bank_param_sub_1.appendChild(doc.createTextNode(getTextExclChild(driver, dom_bank_row_value).trim()));
                                    bank_param.appendChild(bank_param_sub_1);
                                    hasContacts = true;
                                } catch (NotFoundException e) {
                                }
                                try {
                                    bank_param_sub_2 = doc.createElement("link");
                                    bank_param_sub_2.appendChild(doc.createTextNode(dom_bank_row_value.findElement(By.tagName("a")).getAttribute("href")));
                                    bank_param.appendChild(bank_param_sub_2);
                                    hasContacts = true;
                                } catch (NotFoundException e) {
                                }
                                try {
                                    bank_param_sub_3 = doc.createElement("phone");
                                    bank_param_sub_3.appendChild(doc.createTextNode(getTextExclChild(driver, dom_bank_row_value.findElement(By.cssSelector("div[title]")))));
                                    bank_param.appendChild(bank_param_sub_3);
                                    hasContacts = true;
                                } catch (NotFoundException e) {
                                }
                                if (hasContacts) {
                                    bank.appendChild(bank_param);
                                }
                                break;
                            case "Активы нетто":
                                bank_param = doc.createElement("net_assets");
                                bank_param.appendChild(doc.createTextNode(dom_bank_row_value.findElement(By.cssSelector("span")).getText().trim()));
                                bank.appendChild(bank_param);
                                break;
                            case "Чистая прибыль":
                                bank_param = doc.createElement("net_profit");
                                bank_param.appendChild(doc.createTextNode(dom_bank_row_value.findElement(By.cssSelector("span")).getText().trim()));
                                bank.appendChild(bank_param);
                                break;
                            case "Страхование вкладов":
                                bank_param = doc.createElement("deposit_insurance");
                                bank.appendChild(bank_param);
                                break;
                            case "Справка":
                                thatFuckingTable = false;
                                try {
                                    js.executeScript("return showRef('bankiRef');");
                                    WebElement el = dom_bank_row_value.findElement(By.tagName("table"));
                                    thatFuckingTable = true;
                                } catch (Exception e) {
                                }
                                bank_param = doc.createElement("reference");
                                bank_param.appendChild(doc.createTextNode(thatFuckingTable ? excludeLastLine(policy.sanitize(dom_bank_row_value.getText())) : policy.sanitize(dom_bank_row_value.getText())));
                                bank.appendChild(bank_param);
                                break;
                            default:
                                System.out.println("Unknown param: " + tmpStr2);
                                break;
                        }
                    } catch (NotFoundException e) {
                        tmpStr2 = getTextExclChild(driver, info_row.findElement(By.tagName("td"))).trim();
                        if (!tmpStr2.isEmpty()) {
                            bank_param = doc.createElement("info");
                            bank_param.appendChild(doc.createTextNode(policy.sanitize(tmpStr2)));
                            bank.appendChild(bank_param);
                        }
                    }
                }
                try {
                    dom_bank_row_value = dom_bank_card.findElement(By.cssSelector("tr.end li a"));
                    bank_param = doc.createElement("banki_ru_ref");
                    bank_param.appendChild(doc.createTextNode(dom_bank_row_value.getAttribute("href")));
                    bank.appendChild(bank_param);
                } catch (NotFoundException e) {
                }
                rootElement.appendChild(bank);
                Thread.sleep(1000);
            }

            // Пишем напарсеное в XML файл
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File("D:\\BankParser\\file.xml"));
            transformer.transform(source, result);

            //Закрытие драйвера
            driver.quit();

        } catch (Exception e) {
            e.printStackTrace();
        }

        //Закрытие драйвера
        //driver.quit();
        //driver.close();
    }

}
